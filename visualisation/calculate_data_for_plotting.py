import pandas as pd
import os
import numpy as np
# from sub dataframe make data tables used for plotting for aw plot we need to filter more but use the same function


def get_values_per_slice(slice_subset):
    """ calculates the values for each slice
        ----------
        slice_subset : tuple from looping over groupby object


        Returns
        -------
        values_for_plotting : dataframe
        for each slice the measurements required for the plots (list of measurements)

        """
    dataframe_per_slice = slice_subset[1]
    mean_neighbors_per_slice = dataframe_per_slice['neighbors'].mean()
    area_std_per_slice = dataframe_per_slice['area'].std()
    area_mean_per_slice = dataframe_per_slice['area'].mean()
    area_cv_per_slice = area_std_per_slice/area_mean_per_slice
    label_count_per_slice = len(dataframe_per_slice)
    slice_number = int(np.unique(dataframe_per_slice['slice_number']))

    # get values for each neighbor

    label_count_per_neighbor = dataframe_per_slice['neighbors'].value_counts(dropna=False).sort_index()
    label_frequency_per_neighbor = label_count_per_neighbor/label_count_per_slice
    per_neighbor = dataframe_per_slice.groupby('neighbors')
    mean_area_per_neighbor = per_neighbor['area'].mean()
    mean_area_per_neighbor_normalized = mean_area_per_neighbor/area_mean_per_slice
    mean_neighbors_of_neighbors = per_neighbor['neighbors_of_neighbors'].mean()

    temp_data_frame = pd.concat([label_count_per_neighbor, label_frequency_per_neighbor, mean_area_per_neighbor,
                           mean_area_per_neighbor_normalized, mean_neighbors_of_neighbors], axis=1)
    values_per_slice = temp_data_frame.reset_index(level=0)
    values_per_slice.columns = ['neighbors', 'label_count_per_neighbor', 'label_frequency_per_neighbor',
                                'mean_area_per_neighbor', 'mean_area_per_neighbor_normalized',
                                'mean_neighbors_of_neighbors']
    values_per_slice['mean_neighbors_per_slice'] = mean_neighbors_per_slice
    values_per_slice['area_mean_per_slice'] = area_mean_per_slice
    values_per_slice['area_std_per_slice'] = area_std_per_slice
    values_per_slice['area_cv_per_slice'] = area_cv_per_slice
    values_per_slice['label_count_per_slice'] = label_count_per_slice
    values_per_slice['slice_number'] = slice_number

    return values_per_slice


def calculate_plot_values(measurements):
    """ calculates the values needed for the plots
        ----------
        measurements : dataframe
            the measurements from the measurements_from_labelimg script
             possibly as subset to remove segmentation errors

        Returns
        -------
        values_for_plotting : dataframe
        for each slice the measurements required for the plots (list of measurements)

        """
    grouped_per_slice = measurements.groupby('slice_number')
    values_per_slice_list = []
    for slice_subset in grouped_per_slice:
        values_per_slice = get_values_per_slice(slice_subset)
        values_per_slice_list.append(values_per_slice)
    values_for_plotting = pd.concat(values_per_slice_list, ignore_index=True)

    return values_for_plotting


if __name__ == "__main__":

    path_to_data = r'D:\master_thesis_cellsegment\data\make_pipeline'
    os.chdir(path_to_data)
    all_measurements_table = pd.read_csv(os.path.join(path_to_data, 'all_measurements_table.csv'), index_col=[0])
    all_measurements_table_nbg = all_measurements_table[all_measurements_table['original_label'] != 0]
    ind = all_measurements_table_nbg['label_edge_or_background'] == False
    all_measurements_table_full_neighbors = all_measurements_table_nbg[ind]

    subset = all_measurements_table_full_neighbors
    subset = subset[subset['neighbors'] > 2]
    subset = subset[subset['axis'] == 2]
    subset = subset[subset['slice_number'] > 130]
    subset = subset[subset['slice_number'] < 265]




grouped_measurements_full_neighbors = subset.groupby([
        'mask_file', 'dim', 'axis'], dropna=False)
    data_for_plots_full_neighbors_list = []
    for sub_tuple in grouped_measurements_full_neighbors:
        dataframe = sub_tuple[1]
        group_keys = sub_tuple[0]
        file_name = f'{group_keys[0]}_{group_keys[1]}_axis{group_keys[2]}'
        dataframe_for_plots_full_neighbors = calculate_plot_values(dataframe)
        dataframe_for_plots_full_neighbors['file_name'] = file_name
        data_for_plots_full_neighbors_list.append(dataframe_for_plots_full_neighbors)

    dataframe_for_plots_full_neighbors_all = pd.concat(data_for_plots_full_neighbors_list, ignore_index=True)

    subset_full_neighbors_of_neighbors = subset[subset['neighbor_edge_or_background'] == 0]

    grouped_measurements_full_neighbors_of_neighbors = subset_full_neighbors_of_neighbors.groupby(
        ['mask_file', 'dim', 'axis'], dropna=False)

    data_for_plots_full_neighbors_of_neighbors_list = []
    for sub_tuple in grouped_measurements_full_neighbors_of_neighbors:
        dataframe = sub_tuple[1]
        group_keys = sub_tuple[0]
        file_name = f'{group_keys[0]}_{group_keys[1]}_axis{group_keys[2]}'
        dataframe_for_plots_full_neighbors_of_neighbors = calculate_plot_values(dataframe)
        dataframe_for_plots_full_neighbors_of_neighbors['file_name'] = file_name
        data_for_plots_full_neighbors_of_neighbors_list.append(dataframe_for_plots_full_neighbors_of_neighbors)

    dataframe_for_plots_full_neighbors_of_neighbors_all = pd.concat(data_for_plots_full_neighbors_of_neighbors_list,
                                                                    ignore_index=True)




    dataframe_for_plots_full_neighbors_all.to_csv('dataframe_for_plots_full_neighbors_all.csv')
    dataframe_for_plots_full_neighbors_of_neighbors_all.to_csv('dataframe_for_plots_full_neighbors_of_neighbors_all.csv')

    len(np.unique(all_measurements_table_full_neighbors['original_label']))
    len(np.unique(subset_full_neighbors_of_neighbors['original_label']))
    len(np.unique(subset['original_label']))
    index_n = np.unique(dataframe_for_plots_full_neighbors_all['slice_number'], return_index=True)
    index_n = index_n[1]
    temp_n = dataframe_for_plots_full_neighbors_all.loc[index_n]
    temp_n['label_count_per_slice'].mean()

    index_n_n = np.unique(dataframe_for_plots_full_neighbors_of_neighbors_all['slice_number'], return_index=True)
    index_n_n = index_n_n[1]
    temp_n_n = dataframe_for_plots_full_neighbors_of_neighbors_all.loc[index_n_n]
    temp_n_n['label_count_per_slice'].mean()

    h = subset[subset['neighbors'] > 9]
    np.unique(h['original_label'])