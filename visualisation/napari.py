#visualize with napari
import skimage
from skimage.io import imread, imsave
import numpy as np
import napari

#load data
datapath= r'C:\Users\dispiche\Documents\MT\data\data_testing\test_segmentations30.01.22'

datatest_im=imread(r"C:\Users\dispiche\Documents\MT\data\data_testing\datatest_im.tif")
datatest_im_labels=imread(datapath+"\datatest_im_labels.tif")
masks_cyto2=imread(datapath+"\datamask_cell.tif")
masks_cyto2_3D=imread(datapath+"\datamask_cell_3D.tif")
masks_cyto2_st=imread(datapath+"\datamask_cell_st.tif")
masks_cyto2_omni=imread(datapath+"\datamask_omni.tif")
masks_cyto2_omni_3D=imread(datapath+"\datamask_omni_3D.tif")
masks_cyto2_omni_st=imread(datapath+"\datamask_omni_st.tif")
viewer = napari.Viewer()

image_layer = viewer.add_image(datatest_im)
image_labels = viewer.add_labels(datatest_im_labels)
viewer.add_labels(masks_cyto2, name="cell")

viewer.add_labels(np.array(masks_cyto2_3D), name="cell_3D")
viewer.add_labels(np.array(masks_cyto2_st), name="cell_st")


viewer.add_labels(np.array(masks_cyto2_omni), name="omni")
viewer.add_labels(np.array(masks_cyto2_omni_3D), name="omni_3D")
viewer.add_labels(np.array(masks_cyto2_omni_st), name="omni_st")

# expand labels
expanded_st = skimage.segmentation.expand_labels(masks_cyto2_st, distance=2)
viewer.add_labels(expanded_st, name="expanded_st")