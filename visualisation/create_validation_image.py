import numpy as np
import pandas as pd
from skimage.io import imread, imsave
import os
path_to_data = r"C:\Users\dispiche\Documents\MT\data\Meshes_harold"
all_measurements_table = pd.read_csv(os.path.join(path_to_data, "all_measurements_table_harold.csv"),  index_col=[0])
image = imread(os.path.join(path_to_data, "conversion.tif"))
image = np.swapaxes(image, 0, 2)
imsave('conversion_along_2.tif', image)

center_labels_ind = (all_measurements_table['label_edge_or_background'] == False) & (
        all_measurements_table['neighbor_edge_or_background'] == 0) & (
        all_measurements_table['axis'] == 2) & (all_measurements_table['expansion_factor'] == 0)
center_labels = all_measurements_table[center_labels_ind]
grouped_by_slice = center_labels.groupby('slice_number')
grouped_by_slice['original_label']
per_slice_list = []
for slice in grouped_by_slice:
    dataframe = slice[1]

    slice_number = int(np.unique(dataframe['slice_number']))
    original_labels_unique = np.unique(dataframe['original_label'])

    for labels in original_labels_unique:
        image_slice = image[slice_number, :, :]
        image_slice[image_slice == labels] = 0
        image[slice_number, :, :] = image_slice

per_slice_list.append(original_labels_unique)

unique_labels = np.unique(center_labels['original_label'])

imsave("image_harold_only_invalid.tif", image)
