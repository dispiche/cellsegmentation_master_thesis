import pandas as pd
import os
import matplotlib.pyplot as plt


path_to_data = r'D:\master_thesis_cellsegment\data\harold'
all_measurements_table = pd.read_csv(os.path.join(path_to_data, 'all_measurements_table_harold.csv'), index_col=[0])
all_measurements_table_nbg = all_measurements_table[all_measurements_table['original_label'] != 0]
measurements_table_sub = all_measurements_table_nbg[all_measurements_table_nbg['mask_file'] == 'conversion']

measurements_table_sub = measurements_table_sub[measurements_table_sub['label_edge_or_background'] == False]
grouped_measurements_nbg = measurements_table_sub.groupby([
    'mask_file', 'dim', 'axis', 'expansion_factor'], dropna=False)

pred = pd.read_table(os.path.join(path_to_data, 'CV_Hex.txt'), delimiter=" ")
pred = pred.iloc[:, 0:3]
pred.columns = ['hf', 'ql', 'll']

std_area = grouped_measurements_nbg['area'].std()
mean_area = grouped_measurements_nbg['area'].mean()
CV = std_area/mean_area
nf = grouped_measurements_nbg['neighbors'].value_counts(normalize=True, dropna=False).to_frame()
nf.rename(columns={'neighbors': 'neighbor_frequency'}, inplace=True)
nf = nf.reset_index(level='neighbors')
hf = nf[nf['neighbors'] == 6]
bool_list = CV.index.isin(hf.index)

plt.plot(pred['hf'], pred['ll'])
plt.plot(pred['hf'], pred['ql'])
plt.scatter(CV[bool_list],  hf['neighbor_frequency'], marker='o')
plt.ylabel('hexagon_fraction')
plt.xlabel('area_CV')
plt.title('hf_vs_CV_Harold')
label = CV.index.get_level_values(0)
#plt.legend()
plt.savefig(os.path.join(path_to_data, 'hf_vs_CV_Harold.png'))





