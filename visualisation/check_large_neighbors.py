import pandas as pd

harold = pd.read_csv(r"D:\master_thesis_cellsegment\data\harold\conversio_kevin_cleaned\analysis\all_measurements_table_harold_from_kevin_clean.csv")
franzi = pd.read_csv(r"D:\master_thesis_cellsegment\data\my_curation_franzi\analysis_after_correction\ef2\all_measurements_table_my_curation_ef2.csv")

sub_harold = harold[harold['axis'] == 2]
sub_harold = sub_harold[sub_harold['slice_number'] > 80]
sub_harold = sub_harold[sub_harold['slice_number'] < 380]
sub_harold = sub_harold[sub_harold['neighbors'] > 9]

sub_franzi = franzi[franzi['axis'] == 2]
sub_franzi = sub_franzi[sub_franzi['slice_number'] > 130]
sub_franzi = sub_franzi[sub_franzi['slice_number'] < 265]
sub_franzi = sub_franzi[sub_franzi['neighbors'] > 9]