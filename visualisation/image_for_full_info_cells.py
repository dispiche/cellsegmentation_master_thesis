import pandas as pd
import os
import numpy as np
from skimage.io import imread, imsave

path_to_data = r'D:\master_thesis_cellsegment\data\harold\conversio_kevin_cleaned\analysis'
os.chdir(path_to_data)
all_measurements = pd.read_csv(
    'all_measurements_table_harold_from_kevin_clean.csv', index_col=[0])
all_measurements_sub = all_measurements[all_measurements['original_label'] != 0]
all_measurements_sub = all_measurements_sub[all_measurements_sub['mask_file']
                                            == 'Harolds_meshes_voxalized_at_100nm_clean_ef10']
all_measurements_sub = all_measurements_sub[all_measurements_sub['axis'] == 2]

all_measurements_full_n = all_measurements_sub[all_measurements_sub['label_edge_or_background'] == False]
all_measurements_full_n_of_n = all_measurements_full_n[all_measurements_full_n['neighbor_edge_or_background'] == 0]

Harolds_meshes_voxalized_at_100nm_clean_ef10 = imread(
    r"D:\master_thesis_cellsegment\data\harold\conversio_kevin_cleaned\analysis\Harolds_meshes_voxalized_at_100nm_clean_ef10.tif")

input_image = np.swapaxes(Harolds_meshes_voxalized_at_100nm_clean_ef10, 0, 2)
imsave(r'D:\master_thesis_cellsegment\data\harold\conversio_kevin_cleaned\analysis\Sub_images\original.tif', input_image)

full_n = input_image
for slice_number in np.arange(0, len(full_n)):
    image_slice = input_image[slice_number, :, :]
    data_frame_per_slice = all_measurements_full_n[all_measurements_full_n['slice_number'] == slice_number]
    valid_labels_in_slice = data_frame_per_slice['original_label']

    index = np.invert(np.isin(image_slice, valid_labels_in_slice))
    image_slice[index] = 0
    full_n[slice_number] = image_slice


imsave(r'D:\master_thesis_cellsegment\data\harold\conversio_kevin_cleaned\analysis\Sub_images\full_n.tif', full_n)

full_n_of_n = input_image
for slice_number in np.arange(0, len(full_n_of_n)):
    image_slice = input_image[slice_number, :, :]
    data_frame_per_slice = all_measurements_full_n_of_n[all_measurements_full_n_of_n['slice_number'] == slice_number]
    valid_labels_in_slice = data_frame_per_slice['original_label']

    index = np.invert(np.isin(image_slice, valid_labels_in_slice))
    image_slice[index] = 0
    full_n[slice_number] = image_slice


imsave(r'D:\master_thesis_cellsegment\data\harold\conversio_kevin_cleaned\analysis\Sub_images\full_n_of_n.tif', full_n_of_n)