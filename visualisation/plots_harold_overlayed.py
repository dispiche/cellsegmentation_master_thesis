import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np


def get_subdirectory(plot_type):
    """ creates a directory based on the groups provided in the groupby method if not already present
        Parameters
        ----------
        plot_type: str
            will be used to create the name of the plot should ideally reflect the type of plotting that was done

        group_keys : array
            groups specified in the groupby method to generate the subdirectories
        Returns
        -------
        the path to the subdirectory of the plot
        """
    plot_dir = os.path.join(path_to_data, f'{plot_type}')
    if not os.path.isdir(plot_dir):
        os.makedirs(plot_dir)
    return plot_dir

def mean_neigbors(sub_table_tuple, plot_type):

    plot_data = sub_table_tuple[1]
    file_name = sub_table_tuple[0]

    plt.plot(plot_data['slice_number'], plot_data['mean_neighbors_per_slice'])
    mean_neighbors_all_slices = plot_data['mean_neighbors_per_slice'].mean()
    plt.yticks(ticks=np.arange(5.2, 6.4, 0.2))
    plt.title(f'{file_name}')
    plt.grid()
    plt.text(180, 5.8, f'mean = {round(mean_neighbors_all_slices, 2)}', fontsize=12)
    plt.ylabel('mean_number_of_neighbors')
    plt.xlabel('slice_number')
    plot_dir = get_subdirectory(plot_type)
    file_name = f'{plot_type}_{file_name}'
    plt.savefig(os.path.join(plot_dir, file_name + '.png'))
    plt.clf()


def aw_law(sub_table_tuple, plot_type, harold_data_2):
    """ creates a scatter plot of the neighbor counts vs the neighbors of neighbor counts  for a group in
        a grouped dataframe and saves the plot as png in the plot dir created by get _subdirectory

        Parameters
        ----------
        sub_table_tuple: tuple as given when looping over a grouped dataframe created by the pandas groupby method

        plot_type: str
            will be used to create the name of the plot should ideally reflect the type of plotting that was done

        """
    plot_data = sub_table_tuple[1]
    file_name = sub_table_tuple[0]

    n = np.unique(plot_data['neighbors'])
    m, b = np.polyfit(plot_data['neighbors'], plot_data['mean_neighbors_of_neighbors'], 1)
    plt.scatter(plot_data['neighbors'], plot_data['mean_neighbors_of_neighbors'], c=plot_data['slice_number'])
    plt.colorbar()
    plt.scatter(harold_data_2['n'], harold_data_2['m*n'], alpha=0.5, marker='+', c='black')
    plt.plot(n, m*n + b)
    plt.title(f'{file_name}')
    plt.ylabel('neighbors_of_neighbors')
    plt.xlabel('neighbors')
    plt.grid()
    plt.text(4, 20, f'm = {round(m, 2)} b = {round(b, 2)}', fontsize=12)
    plot_dir = get_subdirectory(plot_type)
    file_name = f'{plot_type}_{file_name}'
    plt.savefig(os.path.join(plot_dir, file_name + '.png'))
    plt.clf()

def hf_vs_area_cv(sub_table_tuple, plot_type, pred):
    plot_data = sub_table_tuple[1]
    file_name = sub_table_tuple[0]
    hf_data = plot_data[plot_data['neighbors'] == 6]

    plt.grid()
    plt.plot(pred['hf'], pred['ll'], label="lewis_law")
    plt.plot(pred['hf'], pred['ql'], label="quadratic_law")
    plt.scatter(hf_data['area_cv_per_slice'], hf_data['label_frequency_per_neighbor'], c=hf_data['slice_number'])
    plt.legend()
    plt.colorbar()
    plt.title(f'{file_name}')
    plt.ylabel('hexagon_frequency')
    plt.xlabel('area_cv')
    plot_dir = get_subdirectory(plot_type)
    file_name = f'{plot_type}_{file_name}'
    plt.savefig(os.path.join(plot_dir, file_name + '.png'))
    plt.clf()


def area_vs_neighbornumber(sub_table_tuple, plot_type):
    """ creates a scatter plot of the neighbor counts vs normalized area
     for a group in a grouped dataframe and saves the plot as png in the plot dir created by get_subdirectory

       Parameters
       ----------
       sub_table_tuple: tuple as given when looping over a grouped dataframe created by the pandas groupby method

       plot_type: str
           will be used to create the name of the plot should ideally reflect the type of plotting that was done

       """
    plot_data = sub_table_tuple[1]
    file_name = sub_table_tuple[0]
    n = np.unique(plot_data['neighbors'])
    levis_law = (n-2)/4
    quadratic_law = (n/6)**2
    plt.grid()
    plt.scatter(plot_data['neighbors'], plot_data['mean_area_per_neighbor_normalized'], c=plot_data['slice_number'])
    plt.plot(n, levis_law, label="lewis_law")
    plt.plot(n, quadratic_law, label="quadratic_law")
    plt.legend()
    plt.colorbar()
    plt.title(f'{file_name}')
    plt.ylabel('normalized_area')
    plt.xlabel('neighbors')
    plot_dir = get_subdirectory(plot_type)
    file_name = f'{plot_type}_{file_name}'
    plt.savefig(os.path.join(plot_dir, file_name + '.png'))
    plt.clf()


if __name__ == "__main__":

    # read data

    path_to_data = r'D:\master_thesis_cellsegment\data\harold\my_analysis_overlaid_with_harold'
    os.chdir(path_to_data)

    dataframe_for_plots_full_neighbors_all = pd.read_csv(
        'dataframe_for_plots_full_neighbors_all.csv', index_col=[0])
    dataframe_for_plots_full_neighbors_of_neighbors_all = pd.read_csv(
        'dataframe_for_plots_full_neighbors_of_neighbors_all.csv', index_col=[0])

    harold_data_1 = pd.read_excel('elife-68135-fig4-data1-v2.xls')
    harold_data_2 = pd.read_excel('elife-68135-fig4-data2-v2.xls')
    harold_data_1 = harold_data_1[harold_data_1['dataset'] == 'tube']
    harold_data_2 = harold_data_2[harold_data_2['dataset'] == 'tube']
    harold_data_2['m*n'] = harold_data_2['n']*harold_data_2['m']
    harold_data_3 = pd.read_excel('elife-68135-fig4-data3-v2.xls')


    # make list of tables
    full_neighbors_grouped = dataframe_for_plots_full_neighbors_all.groupby('file_name')
    # make plots (no Background)
    pred = pd.read_table(os.path.join(path_to_data, 'CV_Hex.txt'), delimiter=" ")
    pred = pred.iloc[:, 0:3]
    pred.columns = ['hf', 'ql', 'll']
    for sub_table_tuple in full_neighbors_grouped:
        area_vs_neighbornumber(sub_table_tuple, "area_vs_nn")
        hf_vs_area_cv(sub_table_tuple, 'hf_vs_area_cv', pred)
        mean_neigbors(sub_table_tuple, "mean_neighbors_per_slice")

    full_neighbors_of_neighbors_grouped = dataframe_for_plots_full_neighbors_of_neighbors_all.groupby('file_name')
    for sub_table_tuple in full_neighbors_of_neighbors_grouped:
        aw_law(sub_table_tuple, "aw_law", harold_data_2)



    plt.plot(harold_data_1['layer in um'], harold_data_1['neighbors'])
    harold_mean_neighbors_all_slices = harold_data_1['neighbors'].mean()

    plt.text(5, 5.8, f'mean = {round(harold_mean_neighbors_all_slices, 2)}', fontsize=12)

    plt.title('mean_neighbors_per_section_lung_tube_harold')
    plt.yticks(ticks=np.arange(5.2, 6.4, 0.2))
    plt.grid()
    plt.ylabel('mean_number_of_neighbors')
    plt.xlabel('distance_from_apical(um)')

    plt.savefig(r'D:\master_thesis_cellsegment\data\harold\my_analysis_overlaid_with_harold\mean_neighbors_per_slice\harolds_means.png')
    plt.clf()

