import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LinearRegression
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

path_to_data_franzi = r'D:\master_thesis_cellsegment\data\my_curation_franzi\analysis_after_correction\ef2'
os.chdir(path_to_data_franzi)
plot_data_full_n_franzi = pd.read_csv('dataframe_for_plots_full_neighbors_all.csv', index_col=[0])
plot_data_full_n_of_n_franzi = pd.read_csv('dataframe_for_plots_full_neighbors_of_neighbors_all.csv', index_col=[0])

path_to_data_harold = r'D:\master_thesis_cellsegment\data\harold\conversion_Kevin\analysis'
os.chdir(path_to_data_harold)
plot_data_full_n_harold = pd.read_csv('dataframe_for_plots_full_neighbors_all.csv', index_col=[0])
plot_data_full_n_of_n_harold = pd.read_csv('dataframe_for_plots_full_neighbors_of_neighbors_all.csv', index_col=[0])

os.chdir(r"D:\master_thesis_cellsegment\data\resources_for_thesis\plots_for_thesis")
# Set the default text font size
plt.rc('font', size=14)
# Set the axes title font size
plt.rc('axes', titlesize=20)
# Set the axes labels font size
plt.rc('axes', labelsize=16)
# Set the legend font size
plt.rc('legend', fontsize=16)
# Set the font size of the figure title
plt.rc('figure', titlesize=20)

linewidth = 4


n = np.unique(plot_data_full_n_harold['neighbors'])

levis_law = (n-2)/4
quadratic_law = (n/6)**2

fig_1, axes_1 = plt.subplots(figsize=(10, 5), nrows=1, ncols=2, sharey=True, constrained_layout=True)
axes_1[0].set_title('A. Pseudostratified epithelium', loc='left')
axes_1[0].set_xlabel('neighbor number (n)')
axes_1[0].set_ylabel('normalized area')
harold = axes_1[0].scatter(plot_data_full_n_harold['neighbors'],
                           plot_data_full_n_harold['mean_area_per_neighbor_normalized'],
                           c=plot_data_full_n_harold['slice_number'],
                           s=40)


axes_1[0].plot(n, levis_law, label="Lewis' law", linewidth=linewidth)
axes_1[0].plot(n, quadratic_law, label="Quadratic law", linewidth=linewidth)
axes_1[0].legend(loc=2)


axes_1[1].set_title('B. Transitional epithelium', loc='left')
axes_1[1].scatter(plot_data_full_n_franzi['neighbors'],
                  plot_data_full_n_franzi['mean_area_per_neighbor_normalized'],
                  c=plot_data_full_n_franzi['slice_number'],
                  s=40)
axes_1[1].plot(n, levis_law, linewidth=linewidth)
axes_1[1].plot(n, quadratic_law, linewidth=linewidth)

cbar = fig_1.colorbar(harold, ax=axes_1[1], shrink=0.8)
cbar.set_ticks([])
cbar.set_label('Apical to Basal', labelpad=10, y=0.5)
plt.setp(axes_1, xticks=plot_data_full_n_harold['neighbors'])
fig_1.savefig("Lewis_trans.png")
plt.show()


n = np.unique(plot_data_full_n_of_n_harold['neighbors'])

aboave = 5*n + 8

fig_2, axes_2 = plt.subplots(figsize=(10, 5), nrows=1, ncols=2, sharey=True, constrained_layout=True)
axes_2[0].set_title('A. Pseudostratified epithelium', loc='left')
axes_2[0].set_xlabel('neighbor number (n)')
axes_2[0].set_ylabel('sum of neighbors of neighbors (m*n)')

harold = axes_2[0].scatter(plot_data_full_n_of_n_harold['neighbors'],
                           plot_data_full_n_of_n_harold['mean_neighbors_of_neighbors'],
                           c=plot_data_full_n_of_n_harold['slice_number'],
                           s=40)
axes_2[0].plot(n, aboave, label="Aboav-Weaire's law", linewidth=linewidth)
axes_2[0].legend(loc=2)

n = np.unique(plot_data_full_n_of_n_franzi['neighbors'])

aboave = 5*n + 8

x = np.array(plot_data_full_n_of_n_franzi['neighbors'])
y = np.array(plot_data_full_n_of_n_franzi['mean_neighbors_of_neighbors'])
axes_2[1].set_title('B. Transitional epithelium', loc='left')
axes_2[1].scatter(x, y, c=plot_data_full_n_of_n_franzi['slice_number'], s=40)

model_aboav_franzi = LinearRegression().fit(x.reshape(-1, 1), y)

axes_2[1].plot(n, aboave, linewidth=linewidth)

cbar = fig_2.colorbar(harold, ax=axes_2[1], shrink=0.8)
cbar.set_ticks([])
cbar.set_label('Apical to Basal', labelpad=10, y=0.5)

plt.setp(axes_2, xticks=plot_data_full_n_franzi['neighbors'])

fig_2.savefig("aboav_trans.png")
plt.show()


#cv vs hex

pred = pd.read_table( 'CV_Hex.txt', delimiter=" ")
pred = pred.iloc[:, 0:3]
pred.columns = ['hf', 'ql', 'll']

hf_data_harold = plot_data_full_n_harold[plot_data_full_n_harold['neighbors'] == 6]
hf_data_franzi = plot_data_full_n_franzi[plot_data_full_n_franzi['neighbors'] == 6]


fig_3, axes_3 = plt.subplots(figsize=(10, 5), nrows=1, ncols=2, sharey=True, constrained_layout=True)
axes_3[0].set_title('A. Pseudostratified epithelium', loc='left')
axes_3[0].set_xlabel('area CV')
axes_3[0].set_ylabel('hexagon frequency')
harold = axes_3[0].scatter(hf_data_harold['area_cv_per_slice'],
                           hf_data_harold['label_frequency_per_neighbor'],
                           c=hf_data_harold['slice_number'],
                           s=40)

axes_3[0].plot(pred['hf'], pred['ll'], label="Lewis' law", linewidth=linewidth)
axes_3[0].plot(pred['hf'], pred['ql'], label="Quadratic law", linewidth=linewidth)

axes_3[0].legend(loc=1)


axes_3[1].set_title('B. Transitional epithelium', loc='left')
axes_3[1].set_xlabel('area CV')
axes_3[1].set_ylabel('hexagon frequency')
axes_3[1].scatter(hf_data_franzi['area_cv_per_slice'],
                  hf_data_franzi['label_frequency_per_neighbor'],
                  c=hf_data_franzi['slice_number'],
                  s=40)

axes_3[1].plot(pred['hf'], pred['ll'], label="Lewis' law", linewidth=linewidth)
axes_3[1].plot(pred['hf'], pred['ql'], label="Quadratic law", linewidth=linewidth)

cbar = fig_1.colorbar(harold, ax=axes_3[1], shrink=0.8)
cbar.set_ticks([])
cbar.set_label('Apical to Basal', labelpad=10, y=0.5)
fig_3.savefig("hex_freq_trans.png")
plt.show()



#awparams refrence

aw_params = pd.read_csv("AWparams_marco.csv")
new_params = pd.DataFrame([[4.86, 8.38, 'PSG'],
              [4.73, 8.76, 'PS'],
              [5.02, 8.84, 'T']], columns=['a', 'b', 'name'])

params_combined = pd.DataFrame(columns=['a', 'b', 'name'])
params_combined['a'] = aw_params['a']
params_combined['b'] = aw_params['b']
params_combined['name'] = aw_params['name']
params_combined = pd.concat([params_combined, new_params], ignore_index=True)

fig_4, axes_4 = plt.subplots(figsize=(10, 5), nrows=1, ncols=2)

color = ['#e2f5db', '#e2f5db', '#cee4c8', '#9bdf91',
         '#73bd73', '#169240', '#72c072', '#169240',
         '#02692d', '#096830', '#014218', '#014218',
         '#807db3', '#5f4e97', '#3c0281', '#c7dcf1',
         '#a4c8ea', '#a8cde7', '#8dc9dc', '#57b0dc',
         '#408ebe', '#1473c2', '#04489b', '#064599',
         '#febccc', '#fb0702', '#fbf723', '#808080']


for i, c in zip(range(0, len(params_combined)), color):
    tmp = params_combined.iloc[i, :]
    axes_4[0].scatter(tmp['a'], tmp['b'], s=40, label=tmp['name'], color=color[i])


axes_4[0].set_title("Regression coefficients", loc='left')
axes_4[0].set_xlabel('slope')
axes_4[0].set_ylabel('intercept')

axes_4[1].remove()
legend = fig_4.legend(params_combined['name'], ncol=2, loc='center right')
plt.tight_layout()
fig_4.savefig("awParams.png")
plt.show()


