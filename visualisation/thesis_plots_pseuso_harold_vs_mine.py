import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LinearRegression

#my data pseudo
path_to_data_harold = r'D:\master_thesis_cellsegment\data\harold\conversion_Kevin\analysis'
os.chdir(path_to_data_harold)
plot_data_full_n_harold = pd.read_csv('dataframe_for_plots_full_neighbors_all.csv', index_col=[0])
plot_data_full_n_of_n_harold = pd.read_csv('dataframe_for_plots_full_neighbors_of_neighbors_all.csv', index_col=[0])

os.chdir(r"D:\master_thesis_cellsegment\data\resources_for_thesis\plots_for_thesis")

#data from harolds paper
lewis_harold = pd.read_csv("harlods_data_lewis.csv").dropna()
aboav_harold = pd.read_csv("harolds_data_aboav_weaire.csv").dropna()
hex_freq_harold = pd.read_csv("harolds_data_hex_freq.csv").dropna()

# Set the default text font size
plt.rc('font', size=14)
# Set the axes title font size
plt.rc('axes', titlesize=20)
# Set the axes labels font size
plt.rc('axes', labelsize=16)
# Set the legend font size
plt.rc('legend', fontsize=16)
# Set the font size of the figure title
plt.rc('figure', titlesize=20)

linewidth = 4


#lewis
n = np.unique(plot_data_full_n_harold['neighbors'])

levis_law = (n-2)/4
quadratic_law = (n/6)**2

fig_1, axes_1 = plt.subplots(figsize=(10, 5), nrows=1, ncols=2, sharey=True, constrained_layout=True)

axes_1[0].set_title('A. Gómez et al', loc='left')
axes_1[0].set_xlabel('neighbor number (n)')
axes_1[0].set_ylabel('normalized area')
harold = axes_1[0].scatter(lewis_harold['neighbours'],
                           lewis_harold['normalized_area'],
                           c=lewis_harold['slice'],
                           s=40)



axes_1[0].plot(n, levis_law, label="Lewis' law", linewidth=linewidth)
axes_1[0].plot(n, quadratic_law, label="Quadratic law", linewidth=linewidth)
axes_1[0].legend(loc=2)



axes_1[1].set_title('B. Analysis pipeline', loc='left')
axes_1[1].scatter(plot_data_full_n_harold['neighbors'],
                  plot_data_full_n_harold['mean_area_per_neighbor_normalized'],
                  c=plot_data_full_n_harold['slice_number'],
                  s=40)

axes_1[1].plot(n, levis_law, linewidth=linewidth)
axes_1[1].plot(n, quadratic_law, linewidth=linewidth)


cbar = fig_1.colorbar(harold, ax=axes_1[1], shrink=0.8)
cbar.set_ticks([])
cbar.set_label('Apical to Basal', labelpad=10, y=0.5)

plt.setp(axes_1, xticks=plot_data_full_n_harold['neighbors'])
fig_1.savefig("Lewis_compare.png")

plt.show()


#aboav
n2 = np.unique(aboav_harold['n'])
aboave2 = 5*n2 + 8

fig_2, axes_2 = plt.subplots(figsize=(10, 5), nrows=1, ncols=2, sharey=True, constrained_layout=True)
axes_2[0].set_title('A. Gómez et al', loc='left')
axes_2[0].set_xlabel('neighbor number (n)')
axes_2[0].set_ylabel('sum of neighbors of neighbors (m*n)')

x = np.array(aboav_harold['n'])
y = np.array(aboav_harold['mxn'])

harold = axes_2[0].scatter(x, y, c=aboav_harold['apical to basal'], s=40)
axes_2[0].plot(n2, aboave2, label="Aboav-Weaire's law", linewidth=linewidth)
axes_2[0].legend(loc=2)
model_aboav_paper = LinearRegression().fit(x.reshape(-1, 1), y)

n = np.unique(plot_data_full_n_of_n_harold['neighbors'])
aboave = 5*n + 8

axes_2[1].set_title('B. Analysis pipeline', loc='left')

x = np.array(plot_data_full_n_of_n_harold['neighbors'])
y = np.array(plot_data_full_n_of_n_harold['mean_neighbors_of_neighbors'])

axes_2[1].scatter(x, y, c=plot_data_full_n_of_n_harold['slice_number'], s=40)

model_aboav_harold = LinearRegression().fit(x.reshape(-1, 1), y)

axes_2[1].plot(n, aboave, label="Aboav-Weaire's law", linewidth=linewidth)
axes_2[1].legend(loc=2)

cbar = fig_2.colorbar(harold, ax=axes_2[1], shrink=0.8)
cbar.set_ticks([])
cbar.set_label('Apical to Basal', labelpad=10, y=0.5)

plt.setp(axes_2, xticks=n2)

fig_2.savefig("aboav_compare.png")
plt.show()

#cv vs hex

pred = pd.read_table('CV_Hex.txt', delimiter=" ")
pred = pred.iloc[:, 0:3]
pred.columns = ['hf', 'ql', 'll']

hf_data_harold = plot_data_full_n_harold[plot_data_full_n_harold['neighbors'] == 6]


fig_3, axes_3 = plt.subplots(figsize=(10, 5), nrows=1, ncols=2, sharey=True, constrained_layout=True)
axes_3[0].set_title('A. Gómez et al', loc='left')
axes_3[0].set_xlabel('area CV')
axes_3[0].set_ylabel('hexagon frequency')
harold = axes_3[0].scatter(hex_freq_harold['area_cv'],
                           hex_freq_harold['freq'],
                           c=hex_freq_harold['slice'],
                           s=40)

axes_3[0].plot(pred['hf'], pred['ll'], label="Lewis' law", linewidth=linewidth)
axes_3[0].plot(pred['hf'], pred['ql'], label="Quadratic law", linewidth=linewidth)

axes_3[0].legend(loc=1)


axes_3[1].set_title('B. Analysis pipeline', loc='left')
axes_3[1].set_xlabel('area CV')
axes_3[1].set_ylabel('hexagon frequency')
axes_3[1].scatter(hf_data_harold['area_cv_per_slice'],
                  hf_data_harold['label_frequency_per_neighbor'],
                  c=hf_data_harold['slice_number'],
                  s=40)

axes_3[1].plot(pred['hf'], pred['ll'], label="Lewis' law", linewidth=linewidth)
axes_3[1].plot(pred['hf'], pred['ql'], label="Quadratic law", linewidth=linewidth)

cbar = fig_1.colorbar(harold, ax=axes_3[1], shrink=0.8)
cbar.set_ticks([])
cbar.set_label('Apical to Basal', labelpad=10, y=0.5)
fig_3.savefig("hex_freq_compare.png")
plt.show()


