import os
import numpy as np
from skimage.io import imread, imsave
from skimage.segmentation import expand_labels
import glob


def load_data(file_name, data_path):
    """ loads an image
    Parameters
    ----------
    file_name : str
        the name of the label image file in tif format

    data_path : str
        the path to the directory where the image is located

    Returns
    -------
    the label image as a numpy array
    """
    mask_file_path = os.path.join(data_path, file_name)
    mask_file = imread(mask_file_path)

    return mask_file


if __name__ == "__main__":

    # directory to folder containing images to preprocess

    data_path = r"D:\master_thesis_cellsegment\data\harold\conversio_kevin_cleaned"
    os.chdir(data_path)
    file_names_to_analyze = glob.glob("*.tif")
    expansion_factors = np.arange(1, 21, 1)

    for file_name in file_names_to_analyze:

        mask_file = load_data(file_name, data_path)
        file_name_clean = file_name.split(".")[0]
        for expansion_factor in expansion_factors:
            expanded_image = expand_labels(mask_file, expansion_factor)
            image_name = os.path.join(data_path, 'expanded_labels', f'{file_name_clean}_ef{expansion_factor}.tif')
            imsave(image_name, expanded_image)

