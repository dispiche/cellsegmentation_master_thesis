import pandas as pd
import os
import matplotlib.pyplot as plt


#paper data from harold
data_directory = r"D:\master_thesis_cellsegment\data\resources_for_thesis\data_from_papers"
os.chdir(data_directory)

fig3d1 = pd.read_excel("elife-68135-fig3-data1-v2.xls")
fig4d2 = pd.read_excel("elife-68135-fig4-data2-v2.xls")
areas_harold = fig3d1[fig3d1['dataset'] == 'tube']
m_harold = fig4d2[fig4d2['dataset'] == 'tube']

#lewis law
areas_harold = areas_harold[areas_harold['neighbours'] > 2]
areas_harold = areas_harold[areas_harold['layerB'] > 73]
areas_harold = areas_harold[areas_harold['layerB'] < 203]

grouped_per_slice = areas_harold.groupby(by='layerB')


lewis_law_per_slice_list = []
for slice_subset in grouped_per_slice:
    area_data = slice_subset[1]
    mean_area = area_data['area'].mean()
    area_per_n = area_data.groupby(by='neighbours')
    mean_area_per_n = area_per_n['area'].mean().reset_index(level=0)
    mean_area_per_n['normalized_area'] = mean_area_per_n['area']/mean_area
    mean_area_per_n['slice'] = slice_subset[0]

    lewis_law_per_slice_list.append(mean_area_per_n)

lewis_law = pd.concat(lewis_law_per_slice_list, ignore_index=True)

plt.scatter(lewis_law['neighbours'], lewis_law['normalized_area'], c=lewis_law['slice'])
plt.show()

#aboav-weaire
aboav_weaire = m_harold[m_harold['n'] > 2]
aboav_weaire['mxn'] = aboav_weaire['n'] * aboav_weaire['m']
plt.scatter(aboav_weaire['n'], aboav_weaire['mxn'], c=aboav_weaire['apical to basal'])
plt.show()


#hexfreq
hex_freq_per_slice_list = []
for slice_subset in grouped_per_slice:
    area_data = slice_subset[1]
    mean_area = area_data['area'].mean()
    area_std_per_slice = area_data['area'].std()
    area_cv = area_std_per_slice/mean_area
    label_freq_per_n = area_data.groupby(by='neighbours')
    hex_freq_data = pd.DataFrame([])
    hex_freq_data['counts'] = label_freq_per_n['neighbours'].value_counts(dropna=False).sort_index()
    hex_freq_data = hex_freq_data.reset_index(level=0)

    hex_freq_data['freq'] = hex_freq_data['counts']/hex_freq_data['counts'].sum()
    hex_freq_data['area_cv'] = area_cv
    hex_freq_data['slice'] = slice_subset[0]
    hex_freq_per_slice_list.append(hex_freq_data)


label_freq = pd.concat(hex_freq_per_slice_list, ignore_index=True)
hex_freq = label_freq[label_freq['neighbours'] == 6]
plt.scatter(hex_freq['area_cv'], hex_freq['freq'], c=hex_freq['slice'])
plt.show()

os.chdir(r"D:\master_thesis_cellsegment\data\resources_for_thesis\plots_for_thesis")
lewis_law.to_csv("harlods_data_lewis.csv")
aboav_weaire.to_csv("harolds_data_aboav_weaire.csv")
hex_freq.to_csv("harolds_data_hex_freq.csv")

p = areas_harold[areas_harold['neighbours'] > 9]