import pandas as pd
import os
import matplotlib.pyplot as plt
from matplotlib import rcParams
import numpy as np


path_to_data = r'D:\master_thesis_cellsegment\data\my_curation_franzi\analysis_after_correction\ef2'
os.chdir(path_to_data)
plot_data_full_n = pd.read_csv('dataframe_for_plots_full_neighbors_all.csv', index_col=[0])
plot_data_full_n_of_n = pd.read_csv('dataframe_for_plots_full_neighbors_of_neighbors_all.csv', index_col=[0])


n = np.unique(plot_data_full_n['neighbors'])
levis_law = (n-2)/4
quadratic_law = (n/6)**2
name = r"Lewis' law and Quadratic law"


# Set the default text font size
plt.rc('font', size=16)
# Set the axes title font size
plt.rc('axes', titlesize=20)
# Set the axes labels font size
plt.rc('axes', labelsize=18)
# Set the legend font size
plt.rc('legend', fontsize=16)
# Set the font size of the figure title
plt.rc('figure', titlesize=20)

plt.scatter(plot_data_full_n['neighbors'], plot_data_full_n['mean_area_per_neighbor_normalized'],
            c=plot_data_full_n['slice_number'], s=100)

cbar = plt.colorbar()
cbar.set_label('Apical to Basal', labelpad=10, y=0.5)
cbar.set_ticks([])
plt.ylabel('Normalized Area')
plt.xlabel('Neighbor Number n')
plt.xticks(plot_data_full_n['neighbors'])

linewidth = 6
plt.plot(n, levis_law, label="Lewis' law", linewidth=linewidth)
plt.plot(n, quadratic_law, label="Quadratic law", linewidth=linewidth)
plt.legend()

plt.gca().set_aspect(2)
plt.savefig(f'{name}.png', bbox_inches="tight")
plt.clf()

name = "Aboav–Weaire Law"

n = np.unique(plot_data_full_n_of_n['neighbors'])

m, b = np.polyfit(plot_data_full_n_of_n['neighbors'], plot_data_full_n_of_n['mean_neighbors_of_neighbors'], 1)
plt.scatter(plot_data_full_n_of_n['neighbors'], plot_data_full_n_of_n['mean_neighbors_of_neighbors'],
            c=plot_data_full_n_of_n['slice_number'], s=100)
plt.plot(n, m*n + b, linewidth=linewidth)
plt.ylabel('m*n ')
plt.xlabel('Neighbor Number n')
plt.xticks(plot_data_full_n_of_n['neighbors'])

plt.text(3, 55, f'a = {round(m, 2)} b = {round(b, 2)}', fontsize=18)
plt.text(5, 22, "AW-law:\nm*n = a*n + b", fontsize=18)
plt.gca().set_aspect(0.17)
plt.savefig(f'{name}.png', bbox_inches="tight")
plt.clf()


pred = pd.read_table(os.path.join(path_to_data, 'CV_Hex.txt'), delimiter=" ")
pred = pred.iloc[:, 0:3]
pred.columns = ['hf', 'ql', 'll']

hf_data = plot_data_full_n[plot_data_full_n['neighbors'] == 6]
name = "Hexagon Frequency vs Area CV"


plt.scatter(hf_data['area_cv_per_slice'], hf_data['label_frequency_per_neighbor'], c=hf_data['slice_number'], s=100)
plt.plot(pred['hf'], pred['ll'], label="Lewis' law", linewidth=linewidth)
plt.plot(pred['hf'], pred['ql'], label="Quadratic law", linewidth=linewidth)
plt.legend()
plt.ylabel('Hexagon Frequency')
plt.xlabel('Area CV')
plt.gca().set_aspect(1.2)
plt.savefig(f'{name}.png', bbox_inches="tight")
plt.clf()