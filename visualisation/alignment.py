import numpy as np
from skimage.io import imread, imsave
import os
os.chdir(r"E:\Dokumente\Studium\Master_thesis_Kevin\franzi_Image_alignement\aligned")
cell_membrane = imread("Cell_membrane.tif")
nucleus = imread("nucleus.tif")

cell_membrane_aligned = np.roll(cell_membrane, shift=7, axis=1)
nucleus_aligned = np.roll(nucleus, shift=9, axis=1)
nucleus_aligned = np.roll(nucleus_aligned, shift=-5, axis=3)
imsave("cell_membrane_aligned.tif", cell_membrane_aligned)

imsave("nucleus_aligned.tif", nucleus_aligned)
