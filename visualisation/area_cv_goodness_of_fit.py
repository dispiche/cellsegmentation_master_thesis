import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt

os.chdir(r'D:\master_thesis_cellsegment\data\my_curation_franzi\analysis_after_correction\ef2')
all_measurements_table = pd.read_csv('all_measurements_table_my_curation_ef2.csv', index_col=[0])
all_measurements_table_nbg = all_measurements_table[all_measurements_table['original_label'] != 0]
ind = all_measurements_table_nbg['label_edge_or_background'] == False
table_franzi = all_measurements_table_nbg[ind]
sub_franzi = table_franzi
sub_franzi = sub_franzi[sub_franzi['axis'] == 2]
sub_franzi = sub_franzi[sub_franzi['neighbors'] > 2]
#sub_franzi = sub_franzi[sub_franzi['neighbors'] < 10]
sub_franzi = sub_franzi[sub_franzi['slice_number'] > 130]
sub_franzi = sub_franzi[sub_franzi['slice_number'] < 265]

os.chdir(r'D:\master_thesis_cellsegment\data\harold\conversion_Kevin\analysis')
all_measurements_table = pd.read_csv('all_measurements_table_harold_from_kevin.csv', index_col=[0])
all_measurements_table_nbg = all_measurements_table[all_measurements_table['original_label'] != 0]
ind = all_measurements_table_nbg['label_edge_or_background'] == False
table_harold = all_measurements_table_nbg[ind]
sub_harold = table_harold
sub_harold = sub_harold[sub_harold['axis'] == 2]
sub_harold = sub_harold[sub_harold['neighbors'] > 2]
#sub_harold = sub_harold[sub_harold['neighbors'] < 10]
sub_harold = sub_harold[sub_harold['slice_number'] > 80]
sub_harold = sub_harold[sub_harold['slice_number'] < 380]





def standard_error_of_estimate(observation, prediction):
    tmp = sum((observation-prediction)**2)/(len(observation)-2)
    see = tmp**0.5
    return see

def area_cv_vs_fit(sub):

    area_cv_vs_fit_list = []

    for slice_number in np.unique(sub['slice_number']):
        image_slice = sub[sub['slice_number'] == slice_number]
        area_cv = image_slice['area'].std()/image_slice['area'].mean()

        observation = image_slice['area']/image_slice['area'].mean()
        n = image_slice['neighbors']
        prediction_lewis = (n-2)/4
        prediction_quadratic = (n/6)**2
        see_lewis = standard_error_of_estimate(observation, prediction_lewis)
        see_lewis = see_lewis
        see_quadratic = standard_error_of_estimate(observation, prediction_quadratic)
        see_quadratic = see_quadratic
        area_cv_vs_fit = pd.DataFrame([])
        area_cv_vs_fit['slice_number'] = [slice_number]
        area_cv_vs_fit['area_cv'] = [area_cv]
        area_cv_vs_fit['see_lewis'] = [see_lewis]
        area_cv_vs_fit['see_quadratic'] = [see_quadratic]
        area_cv_vs_fit_list.append(area_cv_vs_fit)

    area_cv_vs_fit_table = pd.concat(area_cv_vs_fit_list, ignore_index=True)

    return area_cv_vs_fit_table

area_cv_vs_fit_franzi = area_cv_vs_fit(sub_franzi)
area_cv_vs_fit_harold = area_cv_vs_fit(sub_harold)
mean_see = (area_cv_vs_fit_harold['see_lewis'].mean() + area_cv_vs_fit_harold['see_quadratic'].mean())/2



plt.scatter(area_cv_vs_fit_harold['area_cv'], area_cv_vs_fit_harold['see_lewis']/mean_see)
plt.scatter(area_cv_vs_fit_harold['area_cv'], area_cv_vs_fit_harold['see_quadratic']/mean_see)
plt.show()

plt.scatter(area_cv_vs_fit_franzi['area_cv'], area_cv_vs_fit_franzi['see_lewis'])
plt.scatter(area_cv_vs_fit_franzi['area_cv'], area_cv_vs_fit_franzi['see_quadratic'])
plt.show()


