import h5py
import numpy as np
from skimage.io import imsave
from skimage.segmentation import expand_labels

filename = r"D:\master_thesis_cellsegment\data\my_curation_franzi\analysis_after_correction\curated_cells_fixing_mistakes_found_in_analysis.h5"

h5file = h5py.File(filename, 'r')
h5file.name
list(h5file.keys())
predictions = np.array(h5file['predictions'])

predictions_ef3 = expand_labels(predictions, 3)


imsave(r"D:\master_thesis_cellsegment\data\my_curation_franzi\analysis_after_correction\ef2\curated_labels_ef2.tif", predictions_ef3)