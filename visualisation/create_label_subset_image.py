import pandas as pd
import os
import numpy as np
from skimage.io import imread, imsave

def create_subset_image(image, included_labels):
    """ this function takes a label image, and an array of label values to keep.
    it creates a new image of the same shape and fills it with the labels in included_labels
        ----------
        image : numpy array
            the label image you wish to subset

        included_labels : numpy array (int)
            the labels which you want to keep as integers


        Returns
        -------
        image_subset : numpy_array
            an image of the original size with all labels not present in the included_labels array replaced with 0
            """

    image_subset = np.zeros(image.shape)

    for label in included_labels:
        image_subset[np.where(image == label)] = label

    return image_subset


if __name__ == "__main__":

    path_to_data = r'D:\master_thesis_cellsegment\data\my_curation_franzi\analysis_after_correction\ef2'
    os.chdir(path_to_data)
    image = imread(os.path.join(path_to_data, "curated_labels_ef2.tif"))
    data = pd.read_csv(os.path.join(path_to_data, 'all_measurements_table_my_curation_ef2.csv'))

    sub_data = data[data['axis'] == 2]
    sub_data = sub_data[sub_data['slice_number'] > 130]
    sub_data = sub_data[sub_data['slice_number'] < 265]
    sub_data = sub_data[sub_data['neighbors'] > 9]

    labels_above_9 = np.unique(sub_data['original_label'])
    image_subset_above_9 = create_subset_image(image, labels_above_9)
    imsave('image_subset_above_9.tif', image_subset_above_9)

