import numpy as np
import trimesh
import glob
import os

# voxel spacing in the z direction in physical units
voxel_dz = 1

# voxel spacing in the y direction in physical units
voxel_dy = 1

# voxel spacing in the z direction in physical units
voxel_dx = 1


# size of the image in pixels
im_width_z = 2
im_width_y = 50
im_width_x = 20
aprox_lowest_z = 365
aprox_lowest_y = 12
aprox_lowest_x = 160

# calculate the coordinates for each voxel
z = np.arange(aprox_lowest_z, aprox_lowest_z+im_width_z, 1)
y = np.arange(aprox_lowest_y, aprox_lowest_y+im_width_y, 1)
x = np.arange(aprox_lowest_x, aprox_lowest_x+im_width_x, 1)
Z, Y, X = np.meshgrid(z, y, x, indexing="xy")
voxel_coordinates = np.column_stack([Z.ravel(), Y.ravel(), X.ravel()])

# get the coordiantes of each voxel in physical units
physical_coordinates = voxel_coordinates * np.array([voxel_dz, voxel_dy, voxel_dx])

# create a list of your meshes and precompute the data you will use for voxelization. That is the label index,
# the axis aligned bounding box, and the trimesh object

mesh_directory = r'D:\master_thesis_cellsegment\data\harold\cell_meshes'
os.chdir(mesh_directory)
meshes = glob.glob("*.obj")
all_trimesh_objects = []
for mesh in meshes:
    trimesh_object = trimesh.load_mesh(mesh)
    all_trimesh_objects.append(trimesh_object)

mesh_data = []
for i, trimesh_object in enumerate(all_trimesh_objects):
    label_index = i + 1
    bounding_box = trimesh_object.bounds
    mesh_data.append((label_index, bounding_box, trimesh_object))


    # make the blank label image
label_image = np.zeros((im_width_z, im_width_y, im_width_x), dtype=np.uint16)

# loop through each voxel and determine which label it belongs to
for voxel_coord, physical_coord in zip(voxel_coordinates, physical_coordinates):
    for label_index, bounding_box, trimesh_obj in mesh_data:
        # first do a fast check if the coordinate is in the bounding box
        in_bounding_box = trimesh.bounds.contains(bounding_box, [physical_coord])[0]

        if in_bounding_box:
            # if it point is in the bounding box for this mesh, do the slower check for if it is inside the actual mesh
            in_mesh = trimesh_obj.contains([physical_coord])[0]
            if in_mesh:
                label_image[voxel_coord[0], voxel_coord[1], voxel_coord[2]] = label_index

