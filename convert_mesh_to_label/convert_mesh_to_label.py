import numpy as np
import trimesh
import pandas as pd
from skimage.io import imsave
from trimesh.voxel import creation
import os
import glob
from skimage.segmentation import expand_labels

def get_voxel_location(mesh):
    """ Voxelizes the mesh using the trimesh-voxel.creation.voxelize() and extracts the x, y, z
     coordinates of the voxels


        Parameters
        ----------
        mesh : trimesh file
            mesh file read by the trimesh package

        Returns
        -------
        points_frame : data_frame

            the x, y, z coordinates of each voxel.
        """
    voxels = trimesh.voxel.creation.voxelize(mesh, pitch=0.1).fill()
    points = voxels.sparse_indices
    origin = voxels.origin*10
    points_transformed = points+origin

    return pd.DataFrame(points_transformed).astype(int)



def attach_label(points_frame, label):
    """ attaches a number for each mesh instance that will act as a label

        Parameters
        ----------
        points_frame : dataframe of the x y z coordinates gathered from get_voxel_location

        label : int
            a number that will be the label for the voxels in the label image

        Returns
        -------
        points_frame : data_frame

            the x, y, z coordinates of each voxel with a label attached.
        """

    points_frame['label'] = int(label)
    return points_frame


def make_list(path_list):
    """ loads the meshes and creates a list of voxel coordinates with a label attached for all meshes

        Parameters
        ----------
        path_list : str
            the list of paths to all mesh objects


        Returns
        -------
        points_frame_list : list
            a list from all points_frames of all meshes
        """
    label = 1
    points_frame_list = []

    for i in np.arange(0, len(path_list), 1):
        mesh_path = path_list[i]
        mesh = load_meshes(mesh_path)
        print(mesh_path)
        print(mesh.is_watertight)
        points_frame = attach_label(get_voxel_location(mesh), label)
        points_frame_label = attach_label(points_frame, label)
        points_frame_list.append(points_frame_label)
        label += 1

    return points_frame_list


def create_matrix(points_frame_list):
    """ creates an empty image and fills it with the generated labels the indices in the points frame lsit
     are assumed to be in the order [Z, Y, X]

        Parameters
        ----------
        points_frame_list : list
            a list from all points_frames of all meshes


        Returns
        -------
        matrix : 3d numpy array
            the label image generated from the meshes
        """
    all_points = pd.concat(points_frame_list)
    all_points = all_points - all_points.min()
    shape = np.array(all_points.max()[0:3])+1
    matrix = np.zeros(shape).astype(dtype='int')
    z = np.array(all_points[0])
    y = np.array(all_points[1])
    x = np.array(all_points[2])
    labels = np.array(all_points['label'])+1
    matrix[z, y, x] = labels

    return matrix


def load_meshes(mesh_path):
    mesh = trimesh.load_mesh(mesh_path)
    return mesh

if __name__ == "__main__":

    mesh_directory = r'D:\master_thesis_cellsegment\data\harold\cell_meshes'
    os.chdir(mesh_directory)
    meshes = glob.glob("*.obj")
    path_list = []
    for mesh in meshes:
        mesh_path = os.path.join(mesh_directory, mesh)
        path_list.append(mesh_path)

    all_points_list = make_list(path_list)

    image = create_matrix(all_points_list)

    imsave('conversion10x.tif', image)

    image_expanded = expand_labels(image, 2)

    imsave('conversion10x_ef2.tif', image_expanded)

    image_expanded = expand_labels(image, 4)

    imsave('conversion10x_ef4.tif', image_expanded)

    image_expanded = expand_labels(image, 10)

    imsave('conversion10x_ef10.tif', image_expanded)
