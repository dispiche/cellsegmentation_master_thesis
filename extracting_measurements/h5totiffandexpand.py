import h5py
import numpy as np
from skimage.io import imsave
import os
from skimage.segmentation import expand_labels

os.chdir(r"D:\master_thesis_cellsegment\data\my_curation_franzi\analysis_after_correction")

f = h5py.File("curated_cells_fixing_mistakes_found_in_analysis.h5", 'r')
f.keys()
data = f.get('predictions')
data_to_numpy = np.array(data)

image_expanded = expand_labels(data_to_numpy, 5)
imsave("curated_labels_ef2.tif", image_expanded)
