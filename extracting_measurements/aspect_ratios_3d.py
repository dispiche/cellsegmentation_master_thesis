import pandas as pd
import os
import matplotlib.pyplot as plt

def get_aspect_ratio_per_label(measurements_3d):
    """ calculates the aspect ratio of the bounding box. the aspec ratio is defined as the longest axis of the bounding box devided by the shoretest axis

    """

    bounding_box_values = measurements_3d[['bbox_width', 'bbox_height', 'bbox_depth']]
    aspect_ratio = bounding_box_values.max(axis=1)/bounding_box_values.min(axis=1)

    return aspect_ratio





if __name__ == "__main__":

    path_to_data = r'D:\master_thesis_cellsegment\data\my_curation_franzi\analysis_after_correction\ef2'
    os.chdir(path_to_data)
    all_measurements_table = pd.read_csv(os.path.join(path_to_data, 'all_measurements_table_my_curation_ef2.csv'),
                                         index_col=[0])
    all_measurements_table_nbg = all_measurements_table[all_measurements_table['original_label'] != 0]
    all_measurements_table_nbg_3d = all_measurements_table_nbg[all_measurements_table_nbg['dim'] == '3d']
    all_measurements_table_nbg_3d_no_edge = all_measurements_table_nbg_3d[
        all_measurements_table_nbg_3d['label_touches_edge'] == False]
    all_measurements_table_nbg_3d_no_edge = all_measurements_table_nbg_3d_no_edge[
        all_measurements_table_nbg_3d_no_edge['area'] > 300]


aspect_ratios_per_label = get_aspect_ratio_per_label(all_measurements_table_nbg_3d_no_edge)
all_measurements_table_nbg_3d_ar = pd.concat((all_measurements_table_nbg_3d_no_edge,
                                              aspect_ratios_per_label.rename('aspect_ratio')), axis=1)



plt.scatter(all_measurements_table_nbg_3d_ar['centroid_x'], all_measurements_table_nbg_3d_ar['aspect_ratio'])
plt.grid()
plt.title('aspect_ratio_of_bounding_box_vs_centroid_position_along_axis_2')
plt.xlabel('centroid_position')
plt.ylabel('aspect_ratio')
plt.savefig(os.path.join(path_to_data, 'aspect_ratios_3d.png'))
plt.clf()


plt.scatter(all_measurements_table_nbg_3d_ar['centroid_x'], all_measurements_table_nbg_3d_ar['area'])
plt.grid()
plt.title('volume_of_cell_vs_centroid_position_along_axis_2')
plt.xlabel('centroid_position')
plt.ylabel('volume')
plt.savefig(os.path.join(path_to_data, 'volumes_3d.png'))
plt.clf()
