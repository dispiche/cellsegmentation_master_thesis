import os
import numpy as np
from skimage.io import imread
from skimage.segmentation import relabel_sequential
import pyclesperanto_prototype as cle
import pandas as pd
import glob

cle.select_device("GTX")


def touches_edge(mask_image):
    """ finds if a label touches the edge of the picture. In 2d the edge is defined as the pixels laying
    on the rectangle surrounding the image. For 3d the edge is defined as the pixels laying on the sides of the cube
    surrounding the image

    Parameters
    ----------
    mask_image : numpy.ndarray
        a label image in form of a numpy array

    Returns
    -------
    array_label_touches_edge : numpy array containing all labels that lay on the edge
    """

    if len(mask_image.shape) == 3:
        z = np.unique(mask_image[0, :, :])
        ze = np.unique(mask_image[-1, :, :])
        y = np.unique(mask_image[:, 0, :])
        ye = np.unique(mask_image[:, -1, :])
        x = np.unique(mask_image[:, :, 0])
        xe = np.unique(mask_image[:, :, -1])
        array_label_touches_edge = np.unique(np.concatenate([z, ze, y, ye, x, xe]))
        # get uniques from the 6 cube sides

    elif len(mask_image.shape) == 2:
        #get uniques from the 4 borders
        y = np.unique(mask_image[0, :])
        ye = np.unique(mask_image[-1, :])
        x = np.unique(mask_image[:, 0])
        xe = np.unique(mask_image[:, -1])
        array_label_touches_edge = np.unique(np.concatenate([y, ye, x, xe]))
    else:
        #raise error
        pass

    return array_label_touches_edge


def get_neighbors_and_neighbors_of_neighbors(label,
                                             touch_matrix_full, array_label_touches_edge, array_label_touches_background):
    """ for a label creates a boolean Value if a direct neighbor of the label
    touches the edge or the background label. 0 is assumed to be the background label.
    for each label counts the number of neighbors and the number of neighbors of the neighbors

        Parameters
        ----------
        label : int
           the number of the label. Used to subset the touch matrix

        touch_matrix_full : the touch matrix extended to be symmetrical

        array_label_touches_edge : numpy array
            all labels that touch the edge of the image

        label_touches_background : numpy array
            boolean if array for all labels is True if the label touches label 0


        Returns
        -------
        array_label_touches_edge : numpy array containing all labels that lay on the edge
        """
    #for label get the neighbor and neighbor of neighbor counts
    touch_matrix_no_0 = touch_matrix_full
    touch_matrix_no_0[0, :] = False
    touch_matrix_no_0[:, 0] = False
    neighbors_no_0 = np.array(np.where(touch_matrix_no_0[label, :]))
    neighbor_count = neighbors_no_0.shape[1]
    neighbors_of_neighbor = sum(sum(touch_matrix_no_0[:, touch_matrix_no_0[label, :]]))
    neighbors_touch_edge = np.isin(neighbors_no_0, array_label_touches_edge).any()
    neighbors_touch_background = np.isin(neighbors_no_0, array_label_touches_background).any()
    neighbor_edge_or_bg = (neighbors_touch_edge | neighbors_touch_background)
    neighbors_of_neighbors_per_label = np.array(
        (neighbors_touch_edge, neighbors_touch_background,
         neighbor_edge_or_bg, neighbor_count, neighbors_of_neighbor))

    return neighbors_of_neighbors_per_label


def touches_edge_or_background(touch_matrix_full, array_label_touches_edge):
    """ for each label creates a boolean Value if the label touches the edge or the background label.
    0 is assumed to be the background label

    Parameters
    ----------
    touch_matrix_full : numpy.ndarray
        a label image in form of a numpy array

    Returns
    -------
    stats_nc : pandas data frame
        Concatenated result from cle.statistics_of_background_and_labelled_pixels and
        the neighbor relations from the touch matrix
    """
    touch_matrix_full = np.array(touch_matrix_full, dtype=bool)
    array_label_touches_edge = array_label_touches_edge
    labels_seq = np.arange(0, len(touch_matrix_full), 1)
    label_touches_edge = np.isin(labels_seq, array_label_touches_edge)
    label_touches_background = touch_matrix_full[0, :]
    label_edge_or_bg = (label_touches_edge | label_touches_background)
    label_touches_edge_or_background_table = np.column_stack((label_touches_edge,
                                                              label_touches_background, label_edge_or_bg))
    array_label_touches_background = np.where(label_touches_background)

    #for label collect neighbor label if any of neighbors in array_touching_background or array_touching_edge

    neighbors_of_neighbors_table = []
    for label in labels_seq:
        neighbors_of_neighbors_per_label = get_neighbors_and_neighbors_of_neighbors(
            label, touch_matrix_full, array_label_touches_edge, array_label_touches_background)
        neighbors_of_neighbors_table.append(neighbors_of_neighbors_per_label)
    neighbors_of_neighbors_table = pd.DataFrame(neighbors_of_neighbors_table)
    touches_edge_or_background_table = pd.concat(
        [pd.DataFrame(label_touches_edge_or_background_table), neighbors_of_neighbors_table],
        axis=1, ignore_index=True)

    touches_edge_or_background_table = touches_edge_or_background_table.rename(
        {0: "label_touches_edge", 1: "label_touches_background", 2: "label_edge_or_background",
         3: "neighbor_touches_edge", 4: "neighbor_touches_background", 5: "neighbor_edge_or_background",
         6: "neighbors", 7: "neighbors_of_neighbors"}, axis=1)
    return touches_edge_or_background_table


def measure_properties(mask_image, original_image):
    """ measures properties of a label image using the cle.statistics_of_background_and_labelled_pixels function.
    uses cle.touch_matrix to generate the touch matrix. The touch matrix is a triangular matrix that contains true
    for the touching labels and is used in touches_edge_or_background and get_neighbors_and_neighbors_of_neighbors
    see https://clij.github.io/clij2-docs/reference for more information on clesperanto.

    Parameters
    ----------
    mask_image : numpy.ndarray
        a label image in form of a numpy array

    original_image: numpy array
        the original raw image used to segment the labels

    Returns
    -------
    stats_nc : pandas data frame
        Concatenated result from cle.statistics_of_background_and_labelled_pixels and
        the neighbor relations from the touch matrix
    """
    mask_image_tuple = relabel_sequential(mask_image)
    mask_image = np.array(mask_image_tuple[0])
    original_labels = np.array(mask_image_tuple[2])
    touch_matrix = cle.generate_touch_matrix(mask_image)
    touch_matrix_full = np.array(touch_matrix, dtype=bool) + np.array(touch_matrix.T, dtype=bool)

    array_label_touches_edge = touches_edge(mask_image)
    touches_edge_or_background_table = touches_edge_or_background(touch_matrix_full, array_label_touches_edge)

    stats = pd.DataFrame(cle.statistics_of_background_and_labelled_pixels(original_image, labelmap=mask_image))

    stats_nc = pd.concat([stats, touches_edge_or_background_table], axis=1)
    stats_nc['original_label'] = original_labels

    return stats_nc


def process_image_3d(mask_image, original_image):
    """ Processes label image of cell segmentation in 3d

    Parameters
    ----------
    mask_image : numpy array
        A 3D label image

    Returns
    -------
    measurements_table_3d : pandas data frame
        Contains for all passed image names and the measurements collected in measure_properties
    """
    measurements_table_3d = measure_properties(mask_image, original_image)
    measurements_table_3d['slice_number'] = 0
    measurements_table_3d.insert(2, 'slice_number', measurements_table_3d.pop('slice_number'))
    measurements_table_3d['dim'] = "3d"

    return measurements_table_3d


def process_image_per_slice(mask_image, original_image, axis):
    """ Processes label image of cell segmentation per Z slice

    Parameters
    ----------
    mask_image : numpy array
        A 3D label image

    original_image: numpy array
        the raw image that was used to segment the labels. Is used to calculate
         intensity statistics in measure properties

    axis : int
        the axis alon which the image will be sliced. Following numpy conventions 0 = Z, 1 = Y, 2 = X

    Returns
    -------
    measurements_table_2d : pandas data frame
         Contains for all passed image names the measurements collected in measure_properties for each slice
    """
    mask_image = np.swapaxes(mask_image, 0, axis)
    original_image = np.swapaxes(original_image, 0, axis)
    measurements_table_2d = []
    count = 0
    for mask_image_slice, original_image_slice in zip(mask_image, original_image):
        measurements_table_slice = measure_properties(mask_image_slice, original_image_slice)
        # add slice number
        measurements_table_slice['slice_number'] = count
        count += 1
        measurements_table_slice['dim'] = "2d"

        measurements_table_2d.append(measurements_table_slice)

    measurements_table_2d = pd.concat(measurements_table_2d, ignore_index=True)
    measurements_table_2d.insert(2, 'slice_number', measurements_table_2d.pop('slice_number'))

    return measurements_table_2d


def load_data(file_name, data_path):
    """ loads an image
    Parameters
    ----------
    file_name : str
        the name of the label image file in tif format

    data_path : str
        the path to the directory where the image is located

    Returns
    -------
    the label image as a numpy array
    """
    mask_file_path = os.path.join(data_path, file_name)
    mask_file = imread(mask_file_path)

    return mask_file


def pipeline(file_name, data_path, original_image):
    """ runs the analysis for a given image over all axis and expansion factors
    Parameters
    ----------
    file_name : str
        the name of the label image file in tif format

    data_path : str
        the path to the directory where the image is located


    Returns
    -------
    A pandas dataframe containing for all measurements in tidy format, where column values don't apply nan was chosen

    """
    mask_file = load_data(file_name, data_path)


    measurements_tables_3d = process_image_3d(mask_file, original_image)
    measurements_tables_all_slices = []
    for i in [0, 1, 2]:
        axis = i
        measurements_tables_slices = process_image_per_slice(mask_file, original_image, axis)
        measurements_tables_slices["axis"] = i
        measurements_tables_all_slices.append(measurements_tables_slices)
    measurements_table_all_slices = pd.concat(measurements_tables_all_slices)
    measurements_tables_3d['axis'] = 0
    measurements_table = pd.concat([measurements_tables_3d, measurements_table_all_slices], ignore_index=True)
    measurements_table['mask_file'] = file_name.split(".")[0]

    return measurements_table


if __name__ == "__main__":

    # directory to folder containing images
    data_path = r"D:\master_thesis_cellsegment\data\harold\conversion_Kevin\analysis"
    original = imread(r"D:\master_thesis_cellsegment\data\harold\conversion_Kevin\analysis\Harolds_meshes_voxalized_at_100nm_ef10.tif")
    os.chdir(data_path)
    file_names_to_analyze = glob.glob("*.tif")

    # analyse

    measurements_tables_files = [pipeline(file_name, data_path, original)
                                 for file_name in file_names_to_analyze]

    # create one big table
    all_measurements_table = pd.concat(measurements_tables_files, ignore_index=True)
    # save results as csv
    all_measurements_table.to_csv('all_measurements_table_test.csv')



