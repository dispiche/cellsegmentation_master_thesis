from stardist.matching import matching
import numpy as np
from skimage.io import imread, imsave
from skimage.transform import rescale
import os
import pandas as pd
import matplotlib.pyplot as plt

os.chdir(r"D:\master_thesis_cellsegment\data\measure_segmentaion_quality")

#rescale grountruth code adapted from rescale_image_plantseg.py by kevin

RAW_IMAGE_PATH = "CleanSegCells.tif"
RAW_IMAGE_VOXEL_SIZE = np.array([0.21, 0.21, 0.21])
RESCALED_IMAGE_VOXEL_SIZE = np.array([0.25, 0.325, 0.325])
OUTPUT_PATH = "CleanSegCells_rescaled.tif"

raw_image = imread(RAW_IMAGE_PATH)
scale_factor = RAW_IMAGE_VOXEL_SIZE / RESCALED_IMAGE_VOXEL_SIZE

rescaled_image = rescale(raw_image, scale_factor, order=0, preserve_range=True, anti_aliasing=False)
imsave(OUTPUT_PATH, rescaled_image)

Franzi_hand_labeled = imread("CleanSegCells_rescaled.tif")
plantseg_curated = imread("curated_labels_ef2.tif")

Franzi_mask = Franzi_hand_labeled > 0
plantseg_curated[np.invert(Franzi_mask)] = 0
label, volume = np.unique(plantseg_curated, return_counts=True)
volumes_per_label = pd.concat([pd.DataFrame(label), pd.DataFrame(volume)], axis=1)
volume_sorted = np.sort(volume)
plt.plot(volume_sorted[0:45])
plt.show()

# I believe the largest label that is false is at 8031 voxels
labels_to_exclude = label[volume < 8031]
for small_label in labels_to_exclude:
    plantseg_curated[plantseg_curated == small_label] = 0


imsave("plantseg_curated_cropped.tif", plantseg_curated)

metrics = matching(Franzi_hand_labeled, plantseg_curated)
