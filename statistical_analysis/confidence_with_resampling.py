def get_point_line_distance(slope, intercept, point):
    """ retturns the shortest distance onf a point to a line
   ----------
   slope : float
       the slope of the line

   intercept : float
       the intercept of the lione
   point : array of shape (1, 2)
        the cooridinates of the point from which you want to measure the distance

   Returns
   -------
   distance : float
        the shortest distance of the point to the line

   """
    point_x = point[0, 0]
    point_y = point[0, 1]

    pred = point_x*slope + intercept
    distance = abs(point_y-pred)
    return distance

def bootstrap_distance_to_mean(params, n, alpha, new_values):
    """ from the data at random takes one value nad calulates the distance to the mean of the remaining values. repeats n number of times
   ----------
   params : dataframe
       the parameters that for which to do the bootstrap as float

   n : int
       the number of iteration for the bootstrap

   alpha : int
        the percentile limit in percent a value between 0 and 100

   new_values : dataframe
        the parameter values for which you want to now the percentile based on the bootstrapping


   Returns
   -------
   percentile_limits : array
        the value limit ot the alpha percentile based on the bootstrapping

   percentile_new_value: float
        the percentile in which the new value falls based on the bootstrapping

   dist_to_new : float
        shortest distance ot the new parameter values to the line fitted for the refrence data
   """
    sampled_distances = []
    for i in range(n):
        # shuffle data
        params_shuffled = shuffle(params)
        # calcualate mean from n-1 points
        slope, intercept = np.polyfit(params_shuffled[1:, 0], params_shuffled[1:, 1], 1)

    # calculate distance from mean in parameter a and b
        point = np.asarray(params_shuffled[:1, :])
        dist = get_point_line_distance(slope, intercept, point)
        sampled_distances.append(dist)

    #take perecentiles

    sorted_distances = sorted(sampled_distances)

    percentile_limits = np.percentile(sorted_distances, alpha)
    # compare to new value
    slope, intercept = np.polyfit(params[:, 0], params[:, 1], 1)
    dist_to_new = get_point_line_distance(slope, intercept, new_values)
    percentile_new_value = stats.percentileofscore(sorted_distances, dist_to_new, kind='rank')

    return percentile_limits, percentile_new_value, dist_to_new



if __name__ == "__main__":
    import pandas as pd
    import os
    from sklearn.utils import shuffle
    from scipy import stats
    import numpy as np
    import math



    data_directory = r"D:\master_thesis_cellsegment\data\resources_for_thesis\data_from_papers"
    os.chdir(data_directory)
    new_values = np.asarray([[5.02, 8.84]])
    AW_params = pd.read_csv("AWparams_marco.csv")

    params = np.asarray(AW_params[['a', 'b']])
    n = 10000
    alpha = 95
    percentile_limits, percentile_new_value, dist_to_new = bootstrap_distance_to_mean(params, n, alpha, new_values)

    #repeat without mutant
    params_no_mutant = params[:-1, :]

    percentile_limits_no_mutant, percentile_new_value_no_mutant, dist_to_new_no_mutant = bootstrap_distance_to_mean(
        params_no_mutant, n, alpha, new_values)


