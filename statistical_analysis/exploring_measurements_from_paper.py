import pandas as pd
import os
import matplotlib.pyplot as plt
import statsmodels.api as sm
import pylab as py

data_directory = r"D:\master_thesis_cellsegment\data\resources_for_thesis\data_from_papers"
os.chdir(data_directory)

fig3d1 = pd.read_excel("elife-68135-fig3-data1-v2.xls")
fig3d2 = pd.read_excel("elife-68135-fig3-data2-v2.xls")
fig4d1 = pd.read_excel("elife-68135-fig4-data1-v2.xls")
fig4d2 = pd.read_excel("elife-68135-fig4-data2-v2.xls")
fig4d3 = pd.read_excel("elife-68135-fig4-data3-v2.xls")

AW_params = pd.read_csv("AWparams_marco.csv")
a = AW_params['a']
b = AW_params['b']

plt.scatter(a, b)
plt.show()

normalized_a = (a-a.mean())/a.std()
sm.qqplot(normalized_a, line='45')
py.show()

normalized_b = (b-b.mean())/b.std()
sm.qqplot(normalized_b, line='45')
py.show()

a_no_mutant = a[:-1]
b_no_mutant = b[:-1]

plt.scatter(a_no_mutant, b_no_mutant)
plt.show()

normalized_a = (a_no_mutant-a_no_mutant.mean())/a_no_mutant.std()
sm.qqplot(normalized_a, line='45')
py.show()

normalized_b = (b_no_mutant-b_no_mutant.mean())/b_no_mutant.std()
sm.qqplot(normalized_b, line='45')
py.show()