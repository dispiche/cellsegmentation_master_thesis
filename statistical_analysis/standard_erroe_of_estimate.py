import pandas as pd
import os


def standard_error_of_estimate(observation, prediction):
    tmp = sum((observation-prediction)**2)/(len(observation)-2)
    see = (tmp**0.5)
    return see


path_to_data_franzi = r'D:\master_thesis_cellsegment\data\my_curation_franzi\analysis_after_correction\ef2'
os.chdir(path_to_data_franzi)
plot_data_full_n_franzi = pd.read_csv('dataframe_for_plots_full_neighbors_all.csv', index_col=[0])
plot_data_full_n_of_n_franzi = pd.read_csv('dataframe_for_plots_full_neighbors_of_neighbors_all.csv', index_col=[0])

path_to_data_harold = r'D:\master_thesis_cellsegment\data\harold\conversion_Kevin\analysis'
os.chdir(path_to_data_harold)
plot_data_full_n_harold = pd.read_csv('dataframe_for_plots_full_neighbors_all.csv', index_col=[0])
plot_data_full_n_of_n_harold = pd.read_csv('dataframe_for_plots_full_neighbors_of_neighbors_all.csv', index_col=[0])

path_to_data_paper =r"D:\master_thesis_cellsegment\data\resources_for_thesis\plots_for_thesis"
os.chdir(path_to_data_paper)
lewis_paper = pd.read_csv("harlods_data_lewis.csv")
aboav_paper = pd.read_csv("harolds_data_aboav_weaire.csv")
hex_freq_paper = pd.read_csv("harolds_data_hex_freq.csv")

os.chdir("D:\master_thesis_cellsegment\data\statistical_comparison")
#standart error of estimate vs theretical predictions

pred_lewis_franzi = (plot_data_full_n_franzi['neighbors']-2)/4

pred_quadratic_franzi = (plot_data_full_n_franzi['neighbors']/6)**2

pred_aboave_waire_franzi = 5*plot_data_full_n_of_n_franzi['neighbors'] + 8

obs_area_franzi = plot_data_full_n_franzi['mean_area_per_neighbor_normalized']

std_obs_area_franzi = obs_area_franzi.std()

obs_n_of_n_franzi = plot_data_full_n_of_n_franzi['mean_neighbors_of_neighbors']

std_obs_n_of_n_franzi = obs_n_of_n_franzi.std()

SEE_lewis_franzi = standard_error_of_estimate(obs_area_franzi, pred_lewis_franzi)

SEE_quadratic_franzi = standard_error_of_estimate(obs_area_franzi, pred_quadratic_franzi)

SEE_aboave_weaire_franzi = standard_error_of_estimate(obs_n_of_n_franzi, pred_aboave_waire_franzi)


pred_lewis_harold = (plot_data_full_n_harold['neighbors']-2)/4

pred_quadratic_harold = (plot_data_full_n_harold['neighbors']/6)**2

pred_aboave_waire_harold = 5*plot_data_full_n_of_n_harold['neighbors'] + 8

obs_area_harold = plot_data_full_n_harold['mean_area_per_neighbor_normalized']

std_obs_area_harold = obs_area_harold.std()

obs_n_of_n_harold = plot_data_full_n_of_n_harold['mean_neighbors_of_neighbors']

std_obs_n_of_n_harold = obs_n_of_n_harold.std()

SEE_lewis_harold = standard_error_of_estimate(obs_area_harold, pred_lewis_harold)

SEE_quadratic_harold = standard_error_of_estimate(obs_area_harold, pred_quadratic_harold)

SEE_aboave_weaire_harold = standard_error_of_estimate(obs_n_of_n_harold, pred_aboave_waire_harold)


pred_lewis_paper = (lewis_paper['neighbours']-2)/4

pred_quadratic_paper = (lewis_paper['neighbours']/6)**2

pred_aboave_waire_paper = 5*aboav_paper['n'] + 8

obs_area_paper = lewis_paper['normalized_area']

std_obs_area_paper = obs_area_paper.std()

obs_n_of_n_paper = aboav_paper['mxn']

std_obs_n_of_n_paper = obs_n_of_n_paper.std()

SEE_lewis_paper = standard_error_of_estimate(obs_area_franzi, pred_lewis_franzi)

SEE_quadratic_paper = standard_error_of_estimate(obs_area_franzi, pred_quadratic_franzi)

SEE_aboave_weaire_paper = standard_error_of_estimate(obs_n_of_n_franzi, pred_aboave_waire_franzi)

# hexfreq std
franzi_hex_freq = plot_data_full_n_franzi[plot_data_full_n_franzi['neighbors'] == 6]
harold_hex_freq = plot_data_full_n_harold[plot_data_full_n_harold['neighbors'] == 6]
std_cv_franzi = franzi_hex_freq['area_cv_per_slice'].std()
std_cv_harold = harold_hex_freq['area_cv_per_slice'].std()
std_cv_paper = hex_freq_paper['area_cv'].std()

std_hex_franzi = franzi_hex_freq['label_frequency_per_neighbor'].std()
std_hex_harold = harold_hex_freq['label_frequency_per_neighbor'].std()
std_hex_paper = hex_freq_paper['freq'].std()
