import numpy as np
import pandas as pd
from skimage.draw import disk
from skimage.io import imsave
import os

os.chdir(r"D:\master_thesis_cellsegment\data\data_testing\testing_pipe")
shape = (61, 61)
img = np.zeros(shape, dtype=np.uint8)

for i in np.arange(1, 5):
    rr, cc = disk((22 + i*9, 40), 6, shape=shape)
    img[rr, cc] = i+6
    i+=1

for i in np.arange(0, 5):
    rr, cc = disk((40, 22 + i*9), 7, shape=shape)
    img[rr, cc] = i+1
    i+=1

rr, cc = disk((49, 49), 3, shape=shape)
img[rr, cc] = 11

rr, cc = disk((49, 31), 7, shape=shape)
img[rr, cc] = 12

rr, cc = disk((31, 31), 6, shape=shape)
img[rr, cc] = 13


imsave('test_img_disks.tif', img)

# expected output table


output_table = pd.DataFrame({'original_label': [0, 1, 2, 3, 4, 5, 7, 9, 10, 11, 12, 13],
                             'neighbor_count_bg': [9, 4, 4, 6, 6, 2, 4, 5, 3, 2, 6, 5],
                             'neighbor_count_nbg': [0, 3, 4, 6, 5, 1, 3, 4, 2, 1, 5, 4],
                             'nn_count_nbg': [0, 13, 18, 25, 15, 5, 15, 18, 9, 5, 19, 16],
                             'touches_edge': [True, False, False, False, False, True,
                                              False, False, True, False, False, False]
                             })
unique, counts = np.unique(img, return_counts=True)
output_table['area'] = counts

output_table.to_csv('output_table_disks.csv')


img2 = np.arange(0, 50).reshape(( 2, 5, 5))
imsave('neighbor_order.tif', img2)