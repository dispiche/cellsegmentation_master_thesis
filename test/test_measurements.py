from skimage.io import imread
from skimage.draw import rectangle
import pandas as pd
from extracting_measurements.measurments_from_labelimg import measure_properties
import numpy as np


def test_measure_properties():
    test_image = imread(r'D:\master_thesis_cellsegment\data\data_testing\testing_pipe\test_img.tif')
    expected_results = pd.read_csv(r'D:\master_thesis_cellsegment\data\data_testing\testing_pipe\output_table.csv')
    original_image = imread(r"D:\master_thesis_cellsegment\data\data_testing\testing_pipe\test_img.tif")
    results_table = measure_properties(test_image, original_image)
    neighbor_count_nbg = results_table['neighbor_count_nbg']
    expected_neighbor_count_nbg = expected_results['neighbor_count_nbg']
    np.testing.assert_allclose(neighbor_count_nbg, expected_neighbor_count_nbg)
    neighbor_count_bg = results_table['neighbor_count_bg']
    expected_neighbor_count_bg = expected_results['neighbor_count_bg']
    np.testing.assert_allclose(neighbor_count_bg, expected_neighbor_count_bg)
    nn_count_nbg = results_table['nn_count_nbg']
    expected_nn_count_nbg = expected_results['nn_count_nbg']
    np.testing.assert_allclose(nn_count_nbg, expected_nn_count_nbg)


def test_label_touches_edge():

    # setup
    labels_for_neighbors = np.arange(1, (7*8)+1, 1)
    matrix_neighbors = labels_for_neighbors.reshape([8, 7])
    matrix_neighbors = np.c_[matrix_neighbors, np.zeros(8)].astype(int)
    matrix_neighbors[-3, 2] = 0

    labels_touching_edge_true = np.sort(np.concatenate([np.arange(0, 8), np.arange(50, 57), np.arange(8, 44, 7)], axis=0))

    # execute
    results_table = measure_properties(matrix_neighbors, matrix_neighbors)
    results_table_touching_edge = results_table[results_table['label_touches_edge']]
    labels_touching_edge_pred = np.sort(results_table_touching_edge['original_label'])

    # verify

    np.testing.assert_allclose(labels_touching_edge_pred, labels_touching_edge_true)


def test_label_touches_background():

    # setup
    labels_for_neighbors = np.arange(1, (7*8)+1, 1)
    matrix_neighbors = labels_for_neighbors.reshape([8, 7])
    matrix_neighbors = np.c_[matrix_neighbors, np.zeros(8)].astype(int)
    matrix_neighbors[-3, 2] = 0
    labels_touching_background_true = np.sort(
        np.concatenate([[31, 37, 39, 45], np.arange(7, 57, 7)], axis=0))

    # execute
    results_table = measure_properties(matrix_neighbors, matrix_neighbors)
    results_table_touching_edge = results_table[results_table['label_touches_background']]
    labels_touching_background_pred = np.sort(results_table_touching_edge['original_label'])

    # verify

    np.testing.assert_allclose(labels_touching_background_pred, labels_touching_background_true)


def test_neighbor_touches_edge():

    # setup
    labels_for_neighbors = np.arange(1, (7*8)+1, 1)
    matrix_neighbors = labels_for_neighbors.reshape([8, 7])
    matrix_neighbors = np.c_[matrix_neighbors, np.zeros(8)].astype(int)
    matrix_neighbors[-3, 2] = 0

    neighbors_touching_edge_true = np.sort(np.unique(np.concatenate(
        [np.arange(1, 17), np.arange(43, 57), np.arange(1, 51, 7), np.arange(2, 52, 7)], axis=0)))

    # execute
    results_table = measure_properties(matrix_neighbors, matrix_neighbors)
    results_table_touching_edge = results_table[results_table['neighbor_touches_edge'] == 1]
    neighbors_touching_edge_pred = np.sort(results_table_touching_edge['original_label'])

    # verify

    np.testing.assert_allclose(neighbors_touching_edge_pred, neighbors_touching_edge_true)


def test_neighbor_touches_background():

    # setup
    labels_for_neighbors = np.arange(1, (7*8)+1, 1)
    matrix_neighbors = labels_for_neighbors.reshape([8, 7])
    matrix_neighbors = np.c_[matrix_neighbors, np.zeros(8)].astype(int)
    matrix_neighbors[-3, 2] = 0

    neighbors_touching_background_true = np.sort(np.unique(np.concatenate(
        [[52, 46, 44, 36, 30, 24, 32, 40], np.arange(6, 56, 7), np.arange(7, 57, 7)], axis=0)))

    # execute
    results_table = measure_properties(matrix_neighbors, matrix_neighbors)
    results_table_touching_edge = results_table[results_table['neighbor_touches_background'] == 1]
    neighbors_touching_background_pred = np.sort(results_table_touching_edge['original_label'])

    # verify

    np.testing.assert_allclose(neighbors_touching_background_true, neighbors_touching_background_pred)


def test_neighbor_counts():

    # setup
    labels_for_neighbors = np.arange(1, (7*8)+1, 1)
    matrix_neighbors = labels_for_neighbors.reshape([8, 7])
    matrix_neighbors = np.c_[matrix_neighbors, np.zeros(8)].astype(int)
    matrix_neighbors[-3, 2] = 0

    neighbor_counts_true = np.array(
        [0, 2, 3, 3, 3, 3, 3, 2, 3, 4, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 4, 3, 3, 4, 3, 4, 4, 4,
         3, 3, 3, 3, 4, 4, 3, 3, 4, 3, 4, 4, 4, 3, 2, 3, 3, 3, 3, 3, 2])

    # execute

    results_table = measure_properties(matrix_neighbors, matrix_neighbors)
    neighbor_counts_pred = np.array(results_table['neighbors'])

    # verify

    np.testing.assert_allclose(neighbor_counts_true, neighbor_counts_pred)

def test_neighbor_of_neighbor_counts():


    # setup
    labels_for_neighbors = np.arange(1, (7*8)+1, 1)
    matrix_neighbors = labels_for_neighbors.reshape([8, 7])
    matrix_neighbors = np.c_[matrix_neighbors, np.zeros(8)].astype(int)
    matrix_neighbors[-3, 2] = 0

    neighbor_of_neighbor_counts_true = np.array([0, 6, 9, 10, 10, 10, 9, 6, 9, 14, 15, 15, 15, 14, 9, 10, 15, 16, 16,
                                                 16, 15, 10, 10, 15, 15, 16, 16, 15, 10, 10, 13, 12, 14, 16, 15, 10,
                                                 9, 11, 12, 15, 15, 10, 9, 12, 11, 13, 15, 14, 9, 6, 9, 9, 10, 10, 9, 6])

    # execute

    results_table = measure_properties(matrix_neighbors, matrix_neighbors)
    neighbors_of_neighbors_counts_pred = np.array(results_table['neighbors_of_neighbors'])


    # verify

    np.testing.assert_allclose(neighbor_of_neighbor_counts_true, neighbors_of_neighbors_counts_pred)


def test_area():

    # set up
    img = np.zeros((6, 6), dtype=np.uint8)
    start = (3, 3)
    rr, cc = rectangle(start, extent=(2, 2))
    img[rr, cc] = 1
    rr, cc = rectangle(start, extent=(-3, 2))
    img[rr, cc] = 2
    rr, cc = rectangle(start, extent=(-2, -2))
    img[rr, cc] = 3
    rr, cc = rectangle(start, extent=(3, -3))
    img[rr, cc] = 4

    print(img)

    area_true = np.array([13, 4, 6, 4, 9])

    # execute
    results_table = measure_properties(img, img)
    area_pred = np.array(results_table['area'])

    # verify
    np.testing.assert_allclose(area_true, area_pred)
