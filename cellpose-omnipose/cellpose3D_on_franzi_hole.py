import os
from skimage.io import imread, imsave
import numpy as np
import cellpose
from cellpose import models, core

raw_image = imread(r"C:\Users\dispiche\Documents\MT\data\my_curation_franzi\plantseg_20220324\GFP_raw-substack.tif")
model_cyto2 = cellpose.models.Cellpose(model_type='cyto2')

masks_cyto2_3D, flows_cyto2_3D, styles_cyto2_3D, diams_omni_3D = model_cyto2.eval(raw_image, diameter=None, do_3D=True)