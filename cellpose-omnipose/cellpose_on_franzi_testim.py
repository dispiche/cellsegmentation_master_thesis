#get logging information

import logging
import pathlib
import sys


cp_dir = pathlib.Path.home().joinpath('.cellpose')
cp_dir.mkdir(exist_ok=True)
log_file = cp_dir.joinpath('run.log')
try:
    log_file.unlink()
except:
    print('creating new log file')
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler(log_file),
        logging.StreamHandler(sys.stdout)
    ]
)
logger = logging.getLogger(__name__)
logger.info(f'WRITING LOG OUTPUT TO {log_file}')

#image processing
import os
import napari
from skimage.io import imread, imsave
import numpy as np
from tifffile import imsave
import cellpose
from cellpose import models, core

use_GPU = core.use_gpu()
print('>>> GPU activated? %d'%use_GPU)

#read in data

path = os.getcwd()
pathsplit = path.rsplit("\\", 2)
datapath = pathsplit[0] + "\\data"
test_im = imread(datapath + "\data_testing\crop_im_Franzi_24.01.22.tif")
test_im_labels = imread(datapath + "\data_testing\crop_label_im_Franzi_24.01.22.tif")
#define model
model_cyto2 = cellpose.models.Cellpose(model_type='cyto2', gpu=True)
model_cyto2_omni = cellpose.models.Cellpose(model_type='cyto2_omni', omni=True, gpu=True)


test_im_inv = 65535 - test_im
im_list = [z_slice for z_slice in test_im]
im_list_test = im_list
im_list_inv = [z_slice for z_slice in test_im_inv]
im_list_inv_test = im_list_test

#run model
masks_cyto2, flows_cyto2, styles_cyto2, diams_cyto2 = model_cyto2.eval(im_list_test, diameter=60, flow_threshold=None)
masks_cyto2_3D, flows_cyto2_3D, styles_cyto2_3D, diams_omni_3D = model_cyto2.eval(np.array(im_list_test), diameter=None, do_3D=True)
masks_cyto2_st, flows_cyto2_st, styles_cyto2_st, diams_cyto2_st = model_cyto2.eval(np.array(im_list_test), diameter=None, do_3D=False, stitch_threshold=0.5)
masks_cyto2_omni, flows_cyto2_omni, styles_cyto2_omni, diams_cyto2_omni = model_cyto2_omni.eval(im_list_test, diameter=None)
masks_cyto2_omni_3D, flows_cyto2_omni_3D, styles_cyto2_omni_3D, diams_cyto2_omni_3D = model_cyto2_omni.eval(np.array(im_list_test), diameter=None, do_3D=True)
masks_cyto2_omni_st, flows_cyto2_omni_st, styles_cyto2_omni_st, diams_cyto2_omni_st = model_cyto2_omni.eval(np.array(im_list_test), diameter=None, do_3D=False, stitch_threshold=0.5)

masks_inv_cyto2_omni, flows_inv_cyto2_omni, styles_inv_cyto2_omni, diams_inv_cyto2_omni = model_cyto2_omni.eval(im_list_inv_test, diameter=None)


imsave(datapath + "test_im.tif", np.array(test_im))
imsave(datapath + "test_im_labels.tif", np.array(test_im_labels))
imsave(datapath + "mask_cell.tif", np.array(masks_cyto2))
imsave(datapath + "mask_cell_3D.tif", np.array(masks_cyto2_3D))
imsave(datapath + "mask_cell_st.tif", np.array(masks_cyto2_st))
imsave(datapath + "mask_omni.tif", np.array(masks_cyto2_omni))
imsave(datapath + "mask_omni_3D.tif", np.array(masks_cyto2_omni_3D))
imsave(datapath + "mask_omni_st.tif", np.array(masks_cyto2_omni_st))

imsave(datapath + "mask_inv_omni.tif", np.array(masks_inv_cyto2_omni))

#visiualize results with napari
#viewer = napari.Viewer()

#image_layer = viewer.add_image(test_im)
#image_labels = viewer.add_labels(test_im_labels)
#viewer.add_labels(np.array(masks_cyto2), name="cell")
#viewer.add_labels(np.array(masks_cyto2_3D), name="cell_3D")
#viewer.add_labels(np.array(masks_cyto2_st), name="cell_st")
#viewer.add_labels(np.array(masks_cyto2_omni), name="omni")
#viewer.add_labels(np.array(masks_cyto2_omni_3D), name="omni_3D")
#viewer.add_labels(np.array(masks_cyto2_omni_st), name="omni_st")

#viewer = napari.Viewer()
#image_layer = viewer.add_image(test_im_inv)
#viewer.add_labels(np.array(masks_inv_cyto2_omni), name="omni")